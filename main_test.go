package main

import (
	"testing"
)

type Test struct {
	negatives Negatives
	err error
}

type Tests []Test


func(t *Test) contains(res string) bool{
	for _, neg := range t.negatives{
		if(neg == res){
			return true
		}
	}

	return false
}

func TestNegativesList(t *testing.T){
	tests := Tests{
		Test{Negatives{"No", "Nej","Never"},nil},
	}

	for _, test := range tests {
		n := &NegativesList{test.negatives}

		i := 0
		for i < 20 {
			r:= n.getRandom()
			if(len(r)<2){
				panic("Random string is not valid")
			}
			if(!test.contains(r)){
				panic("String is not included in teststrings")
			}
			i++
		}

	}
}
