package main


import (
	"log"
	"net/http"
	"math/rand"
	"html/template"
	"strings"
)

type Page struct {
	Title string
}

type NegativesList struct {
	negatives Negatives
}

type Negatives []string

var defaultNegatives = Negatives{
	"No...",
	"You wish",
	"Nowhere near",
	"Why do you want to know?",
	"Any day now",
	"You already know the answer",
	"Your ambition is boundless",
	"In your dreams",
	"More like...garage-scale",
	"Maybe in 2025",
	"Not even on a good day",
	"Ask Mattias \"Slangen\" Lind",
	"Not now, not ever",
	"Still not",
	"No. Asking again won't change anything",
	"Asking one more time won't help",
	"Only in the Twilight Zone",
	"You're more like Netfish",
	"Is this about Kafka?",
	"Maybe if you install Kubernetes...",
	"Not even Netflix are Netflix",
	"Ask a colleague",
	"42",
	"You'd probably know",
	"Nah",
	"Njet",
	"Don't think so",
	"Maybe in the future?",
	"There's always a chance",
	"What do you think?",
	"Maybe add some servers?",
	"Maybe add some more servers?",
	"They still have more servers",
	"It depends",
	"Does it really matter?",
	"It's all in the stars",
	"Not on any planet I know of",
	"No, not really",
	"No",
	"NO",
	"NO!",
	"Don't forget to call mum",
	"It's never to late to change path",
	"Try harder",
	"Still no",
	"Ask again tomorrow",
	"Ask again next century",
	"Is anyone, really?",
	"...",
	"Look it up",
	"You're not THAT popular",
	"Bite my shiny metal ass",
	"Many things, but not that",
}

func (n NegativesList) getRandom() string{
	return n.negatives[rand.Intn(len(n.negatives))]
}

func isNetflix(remoteAddr string) bool {
	if(strings.Contains(remoteAddr,"netflix")) {
		return true
	}
	return false
}


func getTitle(n NegativesList, r *http.Request) string{
	if(isNetflix(r.RemoteAddr)){
		return "Maybe you are..."
	}

	return n.getRandom()

}

func handler(w http.ResponseWriter, r *http.Request) {

	p := &Page{Title: getTitle(NegativesList{defaultNegatives},r)}

	t, _ := template.ParseFiles("index.template")
	t.Execute(w, p)

}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8085", nil))
}
